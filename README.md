# Vehicles Monitor

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.1.4.

Vehicles Monitor is a POC angular based frontend SPA that lists connected vehicles with their status.

### Features
**Database**
- Firebase Real-Time Database is used to store our data 'Vehicles, Clients, Status'
- Vehicles list: `Top Swedish car makes`
- Clients list: `Top Swedish famous actors & acctress`
- Status List: `Selected primary vehicle checks`

**Backend Simulator**
- Backend simulator layer push random status and random vehicle update every periodical-time to firebase database to simulate changes from backend
- Backend listens to firebase real-time status updates
- Backend simulator dispatch updates to redux state layer

**Redux Layer**
- Redux state layer recieve the database once then cashes the data in local storage to utilize performance
- Updated status is pushed and updated in the redux store

**UI Layer**
- Default language used is Swedish, can be changed from the Top Nav Menu
- List page Container has two child components 'Grid, Filter'
- Grid component subscribe for items in Redux Store
- You can toggle Real-Time updates through switch button in the Grid
- You can mix & match filters 'VIN as input, Status as Dropdown & Client as Autocomplete Chips'
- Autocomlete component will always filter the already selected clients
- List in grid will be filtered with every update from the backend
- Logo created using [Tailor Brands](https://www.tailorbrands.com/)
- Notification Service 
- Dashboard displaying real-time count for each status  

[Live Demo](https://vehicles-monitor.firebaseapp.com).

## Techonologies used in project
- Angular Cli
- Angular 7
- RxJs for reactive programing
- @ngrx/store for redux state layer
- @ngrx/store-devtools 
- @ngrx/effects 
- @ngx-translate for Localization
- @angular/material for design
- @angular/flex-layout for layout
- Unit Testing by Jasmine & Karma 
- E2E by Protractor
- firbase real-time database
- firebase deploy
- git-hub repository
- git-lab ci-cd
- docker-hub dock image


## Prerequisites
- [Node.js](https://nodejs.org/en/download/)
- [Angular CLI](https://github.com/angular/angular-cli)

## How to install
- clone repository 
- open directory
- npm install

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
