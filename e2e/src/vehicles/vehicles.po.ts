import { element, browser, by, Key, ElementFinder } from 'protractor';

export class VehiclePage {
	getPage() {
		return browser.get('/vehicles/list');
	}

	getPageTitle() {
		return browser.getTitle();
	}

	getNavBar(): ElementFinder {
        return element(by.tagName('nav'));
    }

	getExpandableFilter() {
		return element(by.css('mat-expansion-indicator ng-tns-c18-4 ng-trigger ng-trigger-indicatorRotate ng-star-inserted'))
	}

	getVINInput() {
		return element(by.id('#mat-input-0'))
	}

	getDashboardMenuItem() {
		return element(by.css('[href="/dashboard"]'));
	}
}
