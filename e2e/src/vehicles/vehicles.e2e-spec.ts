import { by, browser, element } from 'protractor';
import { VehiclePage } from './vehicles.po';

describe('Conduit App E2E Test Suite', () => {
	const page = new VehiclePage();
	describe('page should work fine', () => {
		beforeAll(() => {
			page.getPage();
		});

		it('should have right title', () => {
			page.getPageTitle()
				.then((title: string) => {
					expect(title).toEqual('Vehicles Monitor');
				});
		})

		it('Should locate the nav bar', () => {
			expect(page.getNavBar()).toBeDefined();
		});

		it('should open and view filter component', () => {
			page.getExpandableFilter().click();
			expect(page.getVINInput()).toBeTruthy();
		});

		it('Should redirect to the dashboard page when menu is clicked', () => {
			const anchorDashboard = page.getDashboardMenuItem();
			anchorDashboard.click();
			expect(browser.driver.getCurrentUrl()).toContain('/dashboard');
		});
	
	})
})