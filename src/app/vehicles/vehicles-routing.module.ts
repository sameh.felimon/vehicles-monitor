import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VehiclesListPageComponent } from './pages/vehicles-list-page/vehicles-list-page.component';

const routes: Routes = [
  {
    path: 'list', component: VehiclesListPageComponent
  },
  { path: '', pathMatch: 'full', redirectTo: 'vehicles/list' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VehiclesRoutingModule { }
