import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VehiclesListPageComponent } from './vehicles-list-page.component';
import { FirebaseBackendSimulatorComponent } from 'src/app/core/firebase/firebase-backend-simulator/firebase-backend-simulator.component';
import { VehiclesFilterComponent } from '../../components/vehicles-filter/vehicles-filter.component';
import { VehiclesGridComponent } from '../../components/vehicles-grid/vehicles-grid.component';
import { ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { StoreModule, Store } from '@ngrx/store';
import { VehiclesStateService } from 'src/app/store/vehicles/all-vehicles/state-service';
import { VehiclesActions } from 'src/app/store/vehicles/all-vehicles/actions';
import { FirebaseService } from 'src/app/core/firebase/firebase.service';
import { MatSnackBar } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { of, Observable } from 'rxjs';
import { NotificationService } from 'src/app/core/services/notification.service';
import { vehicleDto } from 'src/app/shared/DTOs/vehicles';

describe('VehiclesListPageComponent', () => {
  let component: VehiclesListPageComponent;
  let fixture: ComponentFixture<VehiclesListPageComponent>;
  let mockVehiclesStateService;
  let mockFirebaseService;
  let mockNotificationService;
  let vehicles;
  let vehiclesSnapShot;
  let clients;
  let status;
  let statusList = [];
  let vehiclesList = [];
  let clientsList = [];

  beforeEach(() => {
    mockVehiclesStateService = jasmine.createSpyObj(['selectItems', 'dispatchFilter', 'dispatcLoadInitial']);
    mockFirebaseService = jasmine.createSpyObj(['getAllClients', 'getAllStatus', 'turnOffNotification', 'getAllVehicles', 'turnOffNotification', 'updateVehicleStatus']);
    mockNotificationService = jasmine.createSpyObj(['default'])
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, TranslateModule.forRoot(), MaterialModule, StoreModule.forRoot({}), BrowserAnimationsModule],
      declarations: [VehiclesListPageComponent, FirebaseBackendSimulatorComponent, VehiclesFilterComponent, VehiclesGridComponent],
      providers: [
        { provide: VehiclesStateService, useValue: mockVehiclesStateService },
        { provide: FirebaseService, useValue: mockFirebaseService },
        { provide : NotificationService, useValue: mockNotificationService },
        Store, VehiclesActions]
    });


    fixture = TestBed.createComponent(VehiclesListPageComponent);
    component = fixture.componentInstance;
    clients = {
      val: () => {
        return [
          {
            "id": 1,
            "name": "Alicia Vikander",
            "address": "Långa Gatan, Djurgården",
            "avatar": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQRfD7N18tcYI6zm-SnQqaqEHD8joHFC5RAPTUBYQx33j448Zz3sVy7qbqhsf8WF-mrXzs"
          },
          {
            "id": 2,
            "name": "Malin Åkerman",
            "address": "Strandvägen, Östermalm",
            "avatar": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQSmqPzq3kP2mNiRFv11FGRvwss-TOwr2pVVZ8R9E9qyDsQd84biLPoZCpJDRq4NTFc8SQ"
          }, {
            "id": 3,
            "name": "Alexander Skarsgård",
            "address": "Svartensgatan, Södermalm",
            "avatar": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSq56Fz8kV2JXyJvxSN0Fm5hN0_KPvP6VoVn-4aCdnYpdbUnDi2V_uvvsSIdx4AtUYKTTQ"
          }
        ];
      }
    };

    vehicles =
      [{
        "id": 1,
        "vin": "AD8027002005948Q87291563",
        "status": 1,
        "regNum": "XFMIZRN1QEM",
        "client": 1,
        "make": "KOENIGSEGG",
        "avatar": "https://i.pinimg.com/236x/ea/7f/12/ea7f12e965b319a447eca8684dddff67--peanut-butter-koenigsegg.jpg"
      }, {
        "id": 2,
        "vin": "KW13GYLG6X7260B8WRJ58G3W21445E",
        "status": 1,
        "regNum": "DIIITMV1",
        "client": 2,
        "make": "VOLVO",
        "avatar": "https://www.roosmalen.nl/vanroosmalen/images/nieuws/volvo-persbericht-verkooprecord.jpg"
      }, {
        "id": 3,
        "vin": "IT36M9002005071UE0W50F76629",
        "status": 2,
        "regNum": "QYKECKW1",
        "client": 3,
        "make": "SAAB",
        "avatar": "https://www.autozin.com/imagesl/027120171109/200_5a047be111a70.jpg"
      }];


    vehiclesSnapShot = {
      val: () => {
        return [{
          "id": 1,
          "vin": "AD8027002005948Q87291563",
          "status": 1,
          "regNum": "XFMIZRN1QEM",
          "client": 1,
          "make": "KOENIGSEGG",
          "avatar": "https://i.pinimg.com/236x/ea/7f/12/ea7f12e965b319a447eca8684dddff67--peanut-butter-koenigsegg.jpg"
        }, {
          "id": 2,
          "vin": "KW13GYLG6X7260B8WRJ58G3W21445E",
          "status": 1,
          "regNum": "DIIITMV1",
          "client": 2,
          "make": "VOLVO",
          "avatar": "https://www.roosmalen.nl/vanroosmalen/images/nieuws/volvo-persbericht-verkooprecord.jpg"
        }, {
          "id": 3,
          "vin": "IT36M9002005071UE0W50F76629",
          "status": 2,
          "regNum": "QYKECKW1",
          "client": 3,
          "make": "SAAB",
          "avatar": "https://www.autozin.com/imagesl/027120171109/200_5a047be111a70.jpg"
        }]
      }
    }

    status = {
      val: () => {
        return [{
          "id": 1,
          "status": "Connected",
          "color": "green"
        }, {
          "id": 2,
          "status": "Disconnected",
          "color": "red"
        }, {
          "id": 3,
          "status": "Oil check",
          "color": "grey"
        }, {
          "id": 4,
          "status": "Engine issue",
          "color": "yellow"
        }];
      }
    }
  })

  it('should set the vehicles variable with data from store', () => {
    mockVehiclesStateService.selectItems.and.returnValue(of(vehicles));
    mockFirebaseService.getAllClients.and.returnValue(of(clients));
    mockFirebaseService.getAllVehicles.and.returnValue(of(vehicles));
    mockFirebaseService.getAllStatus.and.returnValue(of(status));

    fixture.detectChanges();

    component.vehicles$.subscribe(v => vehiclesList = v);

    expect(vehiclesList.length).toBe(3);
  })

  it('should set the clients variable with data from firebase', () => {
    mockFirebaseService.getAllVehicles.and.returnValue(of(vehiclesSnapShot));
    mockFirebaseService.getAllStatus.and.returnValue(of(status));
    mockVehiclesStateService.selectItems.and.returnValue(of(vehicles));
    mockFirebaseService.getAllClients.and.returnValue(of(clients));

    fixture.detectChanges();
    component.clients$.subscribe(c => {
      clientsList = c.val()
    });
    
    expect(clientsList.length).toBe(3);
  })

  it('should set the status variable with data from firebase', () => {
    mockFirebaseService.getAllVehicles.and.returnValue(of(vehiclesSnapShot));
    mockFirebaseService.getAllStatus.and.returnValue(of(status));
    mockVehiclesStateService.selectItems.and.returnValue(of(vehicles));
    mockFirebaseService.getAllClients.and.returnValue(of(clients));

    fixture.detectChanges();

    component.status$.subscribe(s => statusList = s.val());

    expect(statusList.length).toBe(4);
  })

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('onIsRealTimeChanged', () => {
    it('should set the isStartListening input variable to be true when true', () => {
      let boolVal = false;

      component.onChangeRealTime(true);
      component.isStartListening$.subscribe(v => { 
        boolVal = v;
       })

      expect(boolVal).toBe(true)
    })
  })
});
