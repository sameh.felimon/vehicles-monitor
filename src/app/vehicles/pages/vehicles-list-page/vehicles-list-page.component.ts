import { Component, OnInit, OnDestroy, ChangeDetectorRef, Inject, ViewEncapsulation } from '@angular/core';
import { VehiclesStateService } from 'src/app/store/vehicles/all-vehicles/state-service';
import { Observable, of } from 'rxjs';
import { vehicleDto } from 'src/app/shared/DTOs/vehicles';
import { FirebaseService } from 'src/app/core/firebase/firebase.service';
import { filterVehicleDto } from 'src/app/store/vehicles/all-vehicles/state';
import { MatSnackBar } from '@angular/material';
import { MediaMatcher } from '@angular/cdk/layout';
import { NotificationService } from 'src/app/core/services/notification.service';

@Component({
  selector: 'veh-vehicles-list-page',
  templateUrl: './vehicles-list-page.component.html',
  styleUrls: ['./vehicles-list-page.component.scss']
})
export class VehiclesListPageComponent implements OnInit, OnDestroy {
  vehicles$: Observable<vehicleDto[]>;
  clients$: Observable<any>;
  status$: Observable<any>;

  isUpdateRandomVehicles$: Observable<boolean>;
  isStartListening$: Observable<boolean>;
  _isMobile: boolean = false;
  private mobileQuery: MediaQueryList;
  private _mobileQueryListener: () => void;
  private realTimeClass: string;

  constructor(private service: VehiclesStateService,
    private fbService: FirebaseService,
    private notificationService: NotificationService,
    private changeDetectorRef: ChangeDetectorRef,
    private media: MediaMatcher) {

    this.mobileQuery = this.media.matchMedia('(min-width: 768px)');
    this._mobileQueryListener = () => {
      this.changeDetectorRef.detectChanges()
      this._isMobile = !this.mobileQuery.matches;
    }

    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  ngOnInit() {
    this.vehicles$ = this.service.selectItems();
    this.clients$ = this.fbService.getAllClients();
    this.status$ = this.fbService.getAllStatus();
  }

  onChangeRealTime(isRealTime: boolean) {
    this.isStartListening$ = of(isRealTime);
    this.isUpdateRandomVehicles$ = of(isRealTime);

    this.realTimeClass = isRealTime ? 'real-time-green' : 'real-time-red';
    this.notificationService.default('real-time status', isRealTime ? 'On' : 'Off');
  }

  ngOnDestroy(): void {
    this.isStartListening$ = of(false);
    this.isUpdateRandomVehicles$ = of(false);
    if (this.mobileQuery)
      this.mobileQuery.removeListener(this._mobileQueryListener);
  }

  onDispatchUpdates(filter: filterVehicleDto) {
    this.service.dispatchFilter(filter);
  }
}
