import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardPageComponent } from './dashboard-page.component';
import { DashboardComponent } from '../../components/dashboard/dashboard.component';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { VehiclesStateService } from 'src/app/store/vehicles/all-vehicles/state-service';
import { Store, StateObservable, StoreModule } from '@ngrx/store';
import { VehiclesActions } from 'src/app/store/vehicles/all-vehicles/actions';

describe('DashboardPageComponent', () => {
  let component: DashboardPageComponent;
  let fixture: ComponentFixture<DashboardPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot({}), MaterialModule, StoreModule.forRoot({})],
      declarations: [ DashboardPageComponent, DashboardComponent ],
      providers: [VehiclesStateService, Store, VehiclesActions, TranslateService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
