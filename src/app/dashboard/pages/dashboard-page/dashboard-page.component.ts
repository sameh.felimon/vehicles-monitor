import { Component, OnInit, OnChanges } from '@angular/core';
import { VehiclesStateService } from 'src/app/store/vehicles/all-vehicles/state-service';
import { Observable } from 'rxjs';
import { vehicleDto } from 'src/app/shared/DTOs/vehicles';

@Component({
  selector: 'veh-dashboard-page',
  templateUrl: './dashboard-page.component.html',
  styleUrls: ['./dashboard-page.component.scss']
})
export class DashboardPageComponent implements OnInit {
  vehicles$: Observable<vehicleDto[]>;
   conCount: number = 0;
   disconCount: number = 0;
   oilCount: number = 0;
   engCount: number = 0;
  constructor(private service: VehiclesStateService) {
    this.vehicles$ = this.service.selectItems();
   }

  ngOnInit() {
    this.vehicles$.subscribe(v => {
      this.conCount = v.filter(c => c.status == 1).length;
      this.disconCount = v.filter(c => c.status == 2).length;
      this.oilCount = v.filter(c => c.status == 3).length;
      this.engCount = v.filter(c => c.status == 4).length;
    })
  }

}
