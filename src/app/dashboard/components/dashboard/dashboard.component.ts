import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'veh-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  @Input() bgClass: string;
  @Input() icon: string;
  @Input() count: number;
  @Input() label: string;
  @Input() data: number;

  ngOnInit() {
  }

}
