export interface vehicleDto {
    id: number;
    status: number;
    client: number;
    vin: string;
    regNum: string;
    make: string;
    avatar: string;

}