export interface clientDTO {
    id: number;
    name: string;
    avatar: string;
    address: string;
}