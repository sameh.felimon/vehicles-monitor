export interface statusDTO {
    id: number;
    color: string;
    status: string;
}